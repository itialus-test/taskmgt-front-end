import { ref, h } from 'vue';
import { LoadingOutlined } from '@ant-design/icons-vue';

export function useCustomTableLoading() {
  const loadingConfig = ref({
    spinning: true,
    indicator: h(LoadingOutlined, {
      style: {
        fontSize: '100px',
        margin: '-20px -60px',
      },
    }),
    tip: 'loading',
    style: {
      fontSize: '20px',
      padding: '120px 0px',
    },
  });

  return loadingConfig;
}

