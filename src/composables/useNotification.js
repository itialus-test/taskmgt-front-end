import { ref } from 'vue'
import { notification } from 'ant-design-vue';

export function useNotification() {
  const count = ref(0)

  const openNotificationWithIcon = (type,message,duration) => {
    notification[type]({
      message: 'Notification',
      description: message,
      placement: 'bottomLeft', // bottomRight, bottomLeft, topLeft, topRight
      duration: duration ===undefined ? 2 : duration, //set the duration to 1 second
    });
  };

  return {
    count,
    openNotificationWithIcon
  }
}