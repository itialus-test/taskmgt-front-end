export const 
paginations={
    pageSizeOptions:[{
        value: '5',
        label: '5/page',
      },
      {
        value: '10',
        label: '10/page',
      },  {
        value: '20',
        label: '20/page',
      }, {
        value: '50',
        label: '50/page',
      }, {
        value: '100',
        label: '100/page',
      }, {
        value: '200',
        label: '200/page',
      }]     
}