export const 
optionLists={

   YesNo:[{
        value: '',
        label: 'All',
        },
        {
        value: '1',
        label: 'Yes',
        },{
        value: '0',
        label: 'No'
        }],

    ActiveInactive:[{
            value: '',
            label: 'All',
            },
            {
            value: '1',
            label: 'Active',
            },{
            value: '0',
            label: 'Inactive'
            }],

    Statuses:[{
        value: '',
        label: 'All',
        },
        {
        value: '0',
        label: 'Inactive',
        },{
        value: '1',
        label: 'Completed'
        },
        {
        value: '2',
        label: 'Pending',
        },{
        value: '3',
        label: 'On Progress'
        }]      
}