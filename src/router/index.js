import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '../views/DashboardView.vue'
import { cookieData } from '@/constants';
import { useAuthStore } from "@/stores/authStore";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/sign-up',
      name: 'signUp',
      // route level code-splitting
      // this generates a separate chunk (SignUp.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/SignUpView.vue' /* webpackChunkName: "SignUpView" */),      
    },
    {
      path: '/activate-account',
      name: 'activateAccount',
      // route level code-splitting
      // this generates a separate chunk (SignUp.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/ActivateAccountView.vue' /* webpackChunkName: "ActivateAccountView" */),      
    },
    {
      path: '/',
      name: 'defaultPage',     
      //defaultPage is automatically redirected to Task Page
      redirect: '/tasks',
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginMainView.vue' /* webpackChunkName: "LoginMainView" */)
    },
    {
      path: '/forgot-password',
      name: 'forgotPassword',
      component: () => import('../views/ForgotPasswordView.vue' /* webpackChunkName: "ForgotPasswordView" */)
    },
    {
      path: '/reset-password',
      name: 'resetPassword', 
      component: () => import('../views/ResetPasswordView.vue' /* webpackChunkName: "ResetPasswordView" */)
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/ProfileView.vue' /* webpackChunkName: "ProfileView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardView,
      meta : {
        requiresAuth:true
      }
    },   
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('../views/TaskView.vue' /* webpackChunkName: "TaskView" */),
      meta : {
        requiresAuth:true
      }
    },  
    {
      path: '/users',
      name: 'users',
      component: () => import('../views/UserView.vue' /* webpackChunkName: "UserView" */),
      meta : {
        requiresAuth:true
      }
    }, 
    {
      path: '/logout',
      name: 'logout',
      beforeEnter: (to, from) => {

        const { logOut } = useAuthStore();       
        // const { resetAllStores } = useResetAllStores()

        const answer = window.confirm('Are you sure you want to log out?')

        if (!answer) {
          return false}
        else{ 
          console.log('Logging out')
          logOut()
          //then redirect to login page
          router.push('/login')
          //resetAllStores()

         }
      },       
    },
    {
      //if user browse a url that does not exist 
      //then redirect to base url '/'
      path: '/:catchAll(.*)',
      redirect:'/'
    },
  ]
})

router.beforeEach((to, from, next) => {

  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isAuthenticated = $cookies.get(cookieData.accessTokenName)  
  const { checkAccessStillValid } = useAuthStore();

  if (!requiresAuth) {
    next() //just allow user to browse the requested URL
  } else if(requiresAuth && !isAuthenticated){  
    next('/login') //redirect user to login page
  }
  else {  
    checkAccessStillValid(); //check if user access still valid
    next() //allow to browse the requested URL
  }
 
})

export default router
