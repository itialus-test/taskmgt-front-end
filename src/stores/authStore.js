import { ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import axios from "axios";
import { useNotification } from '../composables/useNotification';
import router from '../router'
import { cookieData } from '@/constants';


export const useAuthStore = defineStore('authStore', () => {

 //data *************************************/   
  const formData=reactive({
    email:ref(''),
    password:ref(''),   
  })

  const forgotPasswordFormRequest=reactive({
    email:null,
  })

  const resetPasswordFormRequest=reactive({
    email:null,
    password_reset_code:null,
    new_password:null,
    new_password_confirmation:null

  })

  const changePasswordFormRequest=reactive({
    old_password:null,
    new_password:null,
    new_password_confirmation:null

  })

  const forgotPasswordFormRequestLoading = ref(false)
  const resetPasswordFormRequestLoading = ref(false)
  const changePasswordFormRequestLoading = ref(false)
  const loginFormRequestLoading = ref(false)

  const loading = ref(true)
  const landing_page_url = ref('/task')
  const { openNotificationWithIcon } = useNotification()

  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function logIn(values) {
    loginFormRequestLoading.value = true;
      axios
        .post("/auth/login", this.formData)
        .then((res) => {
          openNotificationWithIcon('success',res.data.message);
          loginFormRequestLoading.value = false;
          console.log('Success:', values);
          console.log('passed router');
          $cookies.set(cookieData.accessTokenName, res.data.access_token);  
          router.push(landing_page_url.value)        
        })
        .catch((err) => {
         openNotificationWithIcon('error',err.response.data.message,5);            
         loginFormRequestLoading.value = false;
        });
  }

  function logOut() {
    $cookies.remove(cookieData.accessTokenName)  
  }

  function checkAccessStillValid(){
    loading.value = true;

      axios
        .post("/auth/check-token-status",null, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })
        // .post("/auth/check-token-status", this.formData)
        .then((res) => {
          //openNotificationWithIcon('success',res.data.message);
          loading.value = false;
          console.log('Access still valid');         
        })
        .catch((err) => {
         //openNotificationWithIcon('error',err.response.data.message);            
         loading.value = false;
         console.log('Access no longer valid'); 
         logOut()

         //then redirect to login page
         router.push('/login')        
        });

  }

  function forgotPassword() {
    forgotPasswordFormRequestLoading.value = true;
      axios
        .post("/auth/forgot-password", forgotPasswordFormRequest)
        .then((res) => {
          openNotificationWithIcon('success',res.data.message,5);
          forgotPasswordFormRequestLoading.value = false;    
          router.push('/reset-password')        
        })
        .catch((err) => {
         openNotificationWithIcon('error',err.response.data.message,5);            
         forgotPasswordFormRequestLoading.value = false;
        });
  }

  function resetPassword() {
    resetPasswordFormRequestLoading.value = true;
      axios
        .post("/auth/reset-password", resetPasswordFormRequest)
        .then((res) => {
          openNotificationWithIcon('success',res.data.message,5);
          resetPasswordFormRequestLoading.value = false;    
          router.push('/login')        
        })
        .catch((err) => {
         openNotificationWithIcon('error',err.response.data.message,5);            
         resetPasswordFormRequestLoading.value = false;
        });
  }

  function changePassword() {
    changePasswordFormRequestLoading.value = true;
      axios       
        .post("/auth/change-password",changePasswordFormRequest, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })
        .then((res) => {
            
          openNotificationWithIcon('success',res.data.message,5);
          changePasswordFormRequestLoading.value = false;       
          logOut()   
          
          router.push('/login') 
        })
        .catch((err) => {
         openNotificationWithIcon('error',err.response.data.message,5);            
         changePasswordFormRequestLoading.value = false;
        });
  }



  return { 
    formData, 
    forgotPasswordFormRequest,
    forgotPasswordFormRequestLoading,
    resetPasswordFormRequest,
    resetPasswordFormRequestLoading,
    changePasswordFormRequest,
    changePasswordFormRequestLoading,
    loginFormRequestLoading,

    forgotPassword,
    resetPassword,
    changePassword,
    logIn, 
    logOut, 
    checkAccessStillValid }
})
