import { ref, reactive, toRaw } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useNotification } from '../composables/useNotification'
import { cookieData, systemMessages } from '@/constants'

export const useUserStore = defineStore('userStore', () => {
  //data *************************************/
  const userDataSource = ref([])
  const userDataSourceMeta = ref([])
  const userDataSourceMetaTotalCache = reactive({})
  const userDataSourceMetaLastPage = reactive({})
  const userOptionListSource = ref([])
  const viewUserFormRequestLoading = ref(false)
  const userExportLoading = ref(false)  
  const userPageButtonEnabled = ref(false)
  const currentSortedColumn = ref(null)
  
  let controller = null
  let viewUserFormRequestCache = reactive({})
  const systemMessage = systemMessages

  const viewUserFormRequest = reactive({
    page: 1,
    per_page: import.meta.env.VITE_DEFAULT_PAGINATION_SIZE,
    filter_allcolumn: null,
    filter_activatewildcard: null,
    filter_id: null,
    filter_first_name: null,
    filter_last_name: null,
    filter_email: null,
    filter_website: null,
    filter_phone: null,
    filter_phone2: null,
    filter_phone3: null,
    filter_fax: null,
    filter_address1: null,
    filter_address2: null,
    filter_zip_postal_code: null,
    filter_city: null,
    filter_subnational_entity: null,
    filter_country: null,
    filter_note: null,
    filter_status: null,
    filter_tag: null,
    filter_created_at: null,

    sort_id: 0,
    sort_first_name: 0,
    sort_last_name: 0,
    sort_email: 0,
    sort_website: 0,
    sort_phone: 0,
    sort_phone2: 0,
    sort_phone3: 0,
    sort_fax: 0,
    sort_address1: 0,
    sort_address2: 0,
    sort_zip_postal_code: 0,
    sort_city: 0,
    sort_subnational_entity: 0,
    sort_country: 0,
    sort_note: 0,
    sort_status: 0,
    sort_tag: 0,
    sort_created_at: 0,

    export_to: '',
    count_only: 0
  })

  const storeUserFormRequest = reactive({
    first_name: null,
    last_name: null,
    email: null,
    website: null,
    phone: null,
    phone2: null,
    phone3: null,
    fax: null,
    address1: null,
    address2: null,
    zip_postal_code: null,
    city: null,
    subnational_entity: null,
    country_id: null,
    note: null,
    status: null,
    tag: null
  })

  const updateUserFormRequest = reactive({})

  const deleteUserFormRequest = reactive({})

  const restoreUserFormRequest = reactive({})

  const userLoading = ref(false)

  //const landing_page_url = ref('/dashboard')
  const { openNotificationWithIcon } = useNotification()

  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/

// Check if the objects have the same content

 function objectsHaveSameContentExcept(obj1, obj2, excludedAttribute) {
    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (const key of keys1) {
      if (key !== excludedAttribute && !key.startsWith('sort_') && obj1[key] !== obj2[key]) {
        return false;
      }
    }

    return true;
 }

 function exportToFile(event) {
  //console.log('burtevent',event.key)
  userExportLoading.value = true

  //set what format to use on export
  viewUserFormRequest.export_to = event.key

  //set the count_only to 0 to indicate that this request
  //is NOT counting but to really get the data results
  viewUserFormRequest.count_only = 0

  axios
    .post('/user', viewUserFormRequest, {
      responseType: 'blob',
      headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
    })
    .then((res) => {
      openNotificationWithIcon('success', systemMessage.exportSuccess)
      const url = URL.createObjectURL(new Blob([res.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', 'user.' + viewUserFormRequest.export_to)
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
      viewUserFormRequest.export_to = ''
      userExportLoading.value = false
    })
    .catch((err) => {
      viewUserFormRequest.export_to = ''
      userExportLoading.value = false
      openNotificationWithIcon('error', err.response.data.message)
    })
}
  
 function clearFilters() { 
  //clear all sorts
  for (const key in viewUserFormRequest) {
    if (key.startsWith('filter_')) {
      viewUserFormRequest[key] = null;
    }
  }
  viewUserFormRequest.value['page'] = 1
 }

  function listUser() {
    viewUserFormRequestLoading.value = true
    userDataSource.value = []
    userDataSourceMeta.value['from'] = '--'
    userDataSourceMeta.value['to'] = '--'
    userDataSourceMeta.value['total'] = '--'

    //set the count_only to 0 to indicate that this request
    //is NOT counting but to really get the data results
    viewUserFormRequest.count_only = 0

    axios
      .post('/user', viewUserFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to userDataSource state

        userDataSource.value = res.data.data
        userDataSourceMeta.value = res.data.meta
        //userDataSourceMeta.value['from'] = userDataSourceMeta.value['from'].toLocaleString()
        //userDataSourceMeta.value['to'] = userDataSourceMeta.value['to'].toLocaleString()        
        //userDataSourceMeta.value['total'] = userDataSourceMeta.value['total'].toLocaleString()
        viewUserFormRequestLoading.value = false
        //openNotificationWithIcon('success',res.data.message);
        listUserResultCount()
      })
      .catch((err) => {
        viewUserFormRequestLoading.value = false
        userDataSourceMeta.value['from'] = '0'
        userDataSourceMeta.value['to'] = '0'
        userDataSourceMeta.value['total'] = '0'
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function listUserResultCount() {
    //console.log('burt cache',viewUserFormRequestCache.page) 
    //console.log('burt request',viewUserFormRequest.page) 
    const haveSameContent = objectsHaveSameContentExcept( toRaw(viewUserFormRequest), viewUserFormRequestCache,"page");
    //console.log('burt same content',haveSameContent)

    if (haveSameContent)
    {
      //openNotificationWithIcon('error', 'Same query request is submitted cancelling execution') 
      userDataSourceMeta.value['total'] = userDataSourceMetaTotalCache.value    
      userDataSourceMeta.value['last_page'] = userDataSourceMetaLastPage.value   
      return;
    }else{
      userDataSourceMeta.value['total'] = '(counting..)'
      //openNotificationWithIcon('success', 'New query request detected proceed to execution')
      viewUserFormRequestCache = { ...toRaw(viewUserFormRequest)};
      userDataSourceMetaLastPage.value = null     
      userDataSourceMetaTotalCache.value = null

    }
    

    userPageButtonEnabled.value = false

    if (controller != null) {
      controller.abort()
      //openNotificationWithIcon('error', 'Current counting aborted, Executing new count..')
      controller = null
    }

    //console.log('Burt',controller)
    controller = new AbortController()

    //set the count_only to 1 to indicate that this request
    //is for counting results only
    viewUserFormRequest.count_only = 1

    axios
      .post('/user', viewUserFormRequest, {
        signal: controller.signal,
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to userDataSourceMeta state
        userDataSourceMeta.value['last_page'] = res.data.last_page
        userDataSourceMetaLastPage.value = res.data.last_page
        userDataSourceMeta.value['total'] = res.data.result_count
        userDataSourceMetaTotalCache.value = userDataSourceMeta.value['total']
        userPageButtonEnabled.value = true
      })
      .catch(() => {
        userPageButtonEnabled.value = true
        //viewUserFormRequestLoading.value = false;
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function storeUser() {
    axios
      .post('/user/store', storeUserFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listUser()        
        const haveSameContent = objectsHaveSameContentExcept( toRaw(viewUserFormRequest), viewUserFormRequestCache,"page");
        if(haveSameContent){
          userDataSourceMetaTotalCache.value++
         }
      })
      .catch((err) => {
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function updateUser(id) {
    axios
      .post(`/user/update/${id}`, updateUserFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listUser()
      })
      .catch((err) => {
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function deleteUser(id) {
    axios
      .get(`/user/delete/${id}`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message)
        listUser()
        const haveSameContent = objectsHaveSameContentExcept( toRaw(viewUserFormRequest), viewUserFormRequestCache,"page");
        if(haveSameContent){
          userDataSourceMetaTotalCache.value--
         }
      })
      .catch((err) => {
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function restoreUser() {}

  function optionListUser() {
    axios
      .get(`/user/option-list`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to categoryDataSource state
        userOptionListSource.value = res.data.data
        //openNotificationWithIcon('success',res.data.message);
        //console.log('burt',userOptionListSource.value)
      })
      .catch((err) => {
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function updateUserProfile(){  
    
    updateUserFormRequestLoading.value = true; 
    //userDataSource.value = []   
      axios
        .post(`/user/update-profile`,updateUserFormRequest.value,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          updateUserFormRequestLoading.value = false;                    
          openNotificationWithIcon('success',res.data.message,5);            
          })
        .catch((err) => {
            updateUserFormRequestLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message,5);
        });   
  }

  function showUserProfile(){  
    
    //userDataSource.value = []   
      axios
        .post(`/user/show-profile`,null,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => {  
           updateUserFormRequest.value=res.data.data   
           console.log('burt',updateUserFormRequest.value['first_name'])   
          //openNotificationWithIcon('success',res.data.message,5);            
          })
        .catch((err) => {
          openNotificationWithIcon('error',err.response.data.message,5);
        });   
  }

  

  return {
    userLoading,
    userDataSource,
    userDataSourceMeta,
    viewUserFormRequest,
    storeUserFormRequest,
    updateUserFormRequest,
    deleteUserFormRequest,
    restoreUserFormRequest,
    userOptionListSource,
    viewUserFormRequestLoading,
    userPageButtonEnabled,
    currentSortedColumn,
    userExportLoading,
    updateUserProfile,
    showUserProfile,
    listUserResultCount,
    listUser,
    storeUser,
    updateUser,
    deleteUser,
    restoreUser,
    optionListUser,
    exportToFile
  }
})
