import { ref, reactive, toRaw } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useNotification } from '../composables/useNotification'
import { cookieData, systemMessages } from '@/constants'


export const useTaskStore = defineStore('taskStore', () => {
  //data *************************************/
  const taskDataSource = ref([])
  const taskDataSourceMeta = ref([])
  const taskOptionListSource = ref([])
  const taskDataSourceMetaTotalCache = reactive({})
  const taskDataSourceMetaLastPage = reactive({})
  const taskExportLoading = ref(false)
  const taskPageButtonEnabled = ref(false)
  const  currentSortedColumn = ref(null);

  let controller = null
  let viewTaskFormRequestCache = reactive({})

  const systemMessage = systemMessages;


  const viewTaskFormRequest = reactive({
    page: 1,
    per_page: import.meta.env.VITE_DEFAULT_PAGINATION_SIZE,
    filter_allcolumn: null,
    filter_activatewildcard: null,
    filter_id: null,
    filter_title: null,
    filter_description: null,
    filter_assign_to: null,
    filter_status: null,
    filter_created_at: null,

    sort_id: 0,
    sort_title: 0,
    sort_description: 0,
    sort_assign_to: 0,
    sort_status: 0,
    sort_created_at: 0,

    export_to: '',
    count_only: 0
  })

  const storeTaskFormRequest = reactive({
    title: null,
    description: null,
    user_id: null,
    status: null
  })
  const updateTaskFormRequest = reactive({
    title: null,
    description: null,
    user_id: null,
    status: null
  })

  const deleteTaskFormRequest = reactive({
    title: null,
    description: null,
    status: null
  })

  const restoreTaskFormRequest = reactive({
    title: null,
    description: null,
    status: null
  })

  const taskLoading = ref(false)
  const viewTaskFormRequestLoading = ref(false)
  const storeTaskFormRequestLoading = ref(false)
  const updateTaskFormRequestLoading = ref(false)
  const deleteTaskFormRequestLoading = ref(false)

  //const landing_page_url = ref('/dashboard')
  const { openNotificationWithIcon } = useNotification()

  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function objectsHaveSameContentExcept(obj1, obj2, excludedAttribute) {
    const keys1 = Object.keys(obj1)
    const keys2 = Object.keys(obj2)

    if (keys1.length !== keys2.length) {
      return false
    }

    for (const key of keys1) {
      if (key !== excludedAttribute && !key.startsWith('sort_') && obj1[key] !== obj2[key]) {
        return false
      }
    }

    return true
  }

  function exportToFile(event) {
    //console.log('burtevent',event.key)
    taskExportLoading.value = true

    //set what format to use on export
    viewTaskFormRequest.export_to = event.key

    //set the count_only to 0 to indicate that this request
    //is NOT counting but to really get the data results
    viewTaskFormRequest.count_only = 0

    axios
      .post('/task', viewTaskFormRequest, {
        responseType: 'blob',
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', systemMessage.exportSuccess)
        const url = URL.createObjectURL(new Blob([res.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', 'task.' + viewTaskFormRequest.export_to)
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
        viewTaskFormRequest.export_to = ''
        taskExportLoading.value = false
      })
      .catch((err) => {
        viewTaskFormRequest.export_to = ''
        taskExportLoading.value = false
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function listTask() {
    viewTaskFormRequestLoading.value = true
    taskDataSource.value = []
    taskDataSourceMeta.value['from'] = '--'
    taskDataSourceMeta.value['to'] = '--'
    taskDataSourceMeta.value['total'] = '--'

    //set the count_only to 0 to indicate that this request
    //is NOT counting but to really get the data results
    viewTaskFormRequest.count_only = 0
    axios
      .post('/task', viewTaskFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to taskDataSource state
        taskDataSource.value = res.data.data
        taskDataSourceMeta.value = res.data.meta
        //taskDataSourceMeta.value['from'] = taskDataSourceMeta.value['from'].toLocaleString()
        //taskDataSourceMeta.value['to'] = taskDataSourceMeta.value['to'].toLocaleString()
        //taskDataSourceMeta.value['total'] = taskDataSourceMeta.value['total'].toLocaleString()
        viewTaskFormRequestLoading.value = false
        //openNotificationWithIcon('success',res.data.message);
        listTaskResultCount()
      })
      .catch((err) => {
        viewTaskFormRequestLoading.value = false
        taskDataSourceMeta.value['from'] = '0'
        taskDataSourceMeta.value['to'] = '0'
        taskDataSourceMeta.value['total'] = '0'
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function listTaskResultCount() {
    //console.log('burt cache',viewTaskFormRequestCache.page)
    //console.log('burt request',viewTaskFormRequest.page)
    const haveSameContent = objectsHaveSameContentExcept(
      toRaw(viewTaskFormRequest),
      viewTaskFormRequestCache,
      'page'
    )
    //console.log('burt same content',haveSameContent)

    if (haveSameContent) {
      //openNotificationWithIcon('error', 'Same query request is submitted cancelling execution')
      taskDataSourceMeta.value['total'] = taskDataSourceMetaTotalCache.value
      taskDataSourceMeta.value['last_page'] = taskDataSourceMetaLastPage.value
      return
    } else {
      taskDataSourceMeta.value['total'] = '(counting..)'
      //openNotificationWithIcon('success', 'New query request detected proceed to execution')
      viewTaskFormRequestCache = { ...toRaw(viewTaskFormRequest) }
      taskDataSourceMetaLastPage.value = null
      taskDataSourceMetaTotalCache.value = null
    }

    taskPageButtonEnabled.value = false

    if (controller != null) {
      controller.abort()
      //openNotificationWithIcon('error', 'Current counting aborted, Executing new count..')
      controller = null
    }

    //console.log('Burt',controller)
    controller = new AbortController()

    //set the count_only to 1 to indicate that this request
    //is for counting results only
    viewTaskFormRequest.count_only = 1

    axios
      .post('/task', viewTaskFormRequest, {
        signal: controller.signal,
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to taskDataSourceMeta state
        taskDataSourceMeta.value['last_page'] = res.data.last_page
        taskDataSourceMetaLastPage.value = res.data.last_page
        taskDataSourceMeta.value['total'] = res.data.result_count
        taskDataSourceMetaTotalCache.value = taskDataSourceMeta.value['total']
        taskPageButtonEnabled.value = true
      })
      .catch(() => {
        taskPageButtonEnabled.value = true
        //viewTaskFormRequestLoading.value = false;
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function storeTask() {
    axios
      .post('/task/store', storeTaskFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listTask()
        const haveSameContent = objectsHaveSameContentExcept(
          toRaw(viewTaskFormRequest),
          viewTaskFormRequestCache,
          'page'
        )
        if (haveSameContent) {
          taskDataSourceMetaTotalCache.value++
        }
      })
      .catch((err) => {
        //taskLoading.value = false;
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function updateTask(id) {
    axios
      .post(`/task/update/${id}`, updateTaskFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listTask()
      })
      .catch((err) => {
        taskLoading.value = false
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function deleteTask(id) {
    //taskDataSource.value = []
    axios
      .get(`/task/delete/${id}`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message)
        listTask()
        const haveSameContent = objectsHaveSameContentExcept(
          toRaw(viewTaskFormRequest),
          viewTaskFormRequestCache,
          'page'
        )
        if (haveSameContent) {
          taskDataSourceMetaTotalCache.value--
        }
      })
      .catch((err) => {
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function optionListTask() {
    axios
      .get(`/task/option-list`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to taskDataSource state
        taskOptionListSource.value = res.data.data
        taskLoading.value = false
        //openNotificationWithIcon('success',res.data.message);
      })
      .catch((err) => {
        taskLoading.value = false
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function restoreTask() {}

  return {
    taskLoading,
    taskOptionListSource,
    taskDataSource,
    taskDataSourceMeta,
    viewTaskFormRequest,
    storeTaskFormRequest,
    updateTaskFormRequest,
    deleteTaskFormRequest,
    restoreTaskFormRequest,
    viewTaskFormRequestLoading,
    storeTaskFormRequestLoading,
    updateTaskFormRequestLoading,
    deleteTaskFormRequestLoading,
    currentSortedColumn,
    taskExportLoading,
    listTask,
    optionListTask,
    storeTask,
    updateTask,
    deleteTask,
    restoreTask,
    exportToFile
  }
})
