<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Itialus Task Manatement App</title>
    <script type="module" crossorigin src="/assets/index-3ea73a94.js"></script>
    <link rel="stylesheet" href="/assets/index-489b9945.css">
  </head>
  <body>
    <div id="app"></div>
    
  </body>
</html>
